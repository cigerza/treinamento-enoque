const Sequelize = require('sequelize');
const databaseConfig = require('../config/database');
const Endereco = require('../app/models/Endereco');
const Usuario = require('../app/models/Usuario');
const EstadoCivil = require('../app/models/EstadoCivil');
const Constituinte = require('../app/models/Constituinte');
const Registro = require('../app/models/Registro');
const models = [Endereco, Usuario, EstadoCivil, Constituinte, Registro];

class Database {
    constructor() {
        this.init()
    }

    init() {
        this.connection = new Sequelize(databaseConfig);
        models
            .map((model) => model.init(this.connection))
            .map(
                (model) => model.associate && model.associate(this.connection.models)
        );
    }
}

module.exports = new Database();