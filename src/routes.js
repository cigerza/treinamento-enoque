const express = require('express');

const InfoController = require('./app/controllers/InfoController');
const EnderecoController = require('./app/controllers/EnderecoController');
const UsuarioController = require("./app/controllers/UsuarioController");
const SessionController = require("./app/controllers/SessionController");
const EstadoCivilController = require("./app/controllers/EstadoCivilController");
const ConstituinteController = require("./app/controllers/ConstituinteController");
const RegistroController = require('./app/controllers/RegistroController');

const authMiddleware = require("./app/middlewares/auth");

const routes = express.Router();

routes.get('/', InfoController.index);

// Rotas de endereço
routes.post('/enderecos', EnderecoController.store);
routes.get('/enderecos/:id', EnderecoController.show);
routes.put('/enderecos/:id', EnderecoController.update);


// Rotas de usuários
routes.post('/usuarios', UsuarioController.store);
routes.get('/usuarios/:id', UsuarioController.show);
routes.get('/usuarios/', UsuarioController.index);

// Rota de Sessão
routes.post("/sessions", SessionController.store);

// Rota de autenticação => o que vem abaixo, precisa ser autorizado
routes.use(authMiddleware);

routes.get('/enderecos', EnderecoController.index);

// Rotas de estado civil
routes.post("/estadocivil", EstadoCivilController.store);
routes.get("/estadocivil/:id", EstadoCivilController.show);
routes.put("/estadocivil/:id", EstadoCivilController.update);

// Rotas de constituintes
routes.post("/constituintes", ConstituinteController.store);
routes.get("/constituintes/:id", ConstituinteController.show);
routes.put("/constituintes/:id", ConstituinteController.update);
routes.get('/constituintes', ConstituinteController.index);

// Rotas de registros
routes.post("/registros", RegistroController.store);
routes.get("/registros/:id", RegistroController.show);
routes.put("/registros/:id", RegistroController.update);
routes.get('/registros', RegistroController.index);

module.exports = routes;