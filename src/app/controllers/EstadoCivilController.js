const EstadoCivil = require('../models/EstadoCivil');

class EstadoCivilController {
    async store(req, res) {
        const { nome } = req.body;
        const estadocivil = await EstadoCivil.create({ nome });
        return res.status(200).json(estadocivil);
    }
    
    async show(req, res) {
        const { id } = req.params;        
        const estadocivil = await EstadoCivil.findByPk(id);

        return res.status(200).json(estadocivil);
    }

    async update(req, res) {
        const { id } = req.params;
        const { nome } = req.body;
        const estadocivil = await EstadoCivil.findByPk(id);

        const Uestadocivil = await estadocivil.update({ nome });
        return res.status(200).json(Uestadocivil);
    }
}

module.exports = new EstadoCivilController();