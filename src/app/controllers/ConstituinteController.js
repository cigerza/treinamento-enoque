const Constituinte = require('../models/Constituinte');
const Endereco = require('../models/Endereco');
const EstadoCivil = require('../models/EstadoCivil');

class ConstituinteController {
    async store(req, res) {
        const { nome, rg, cpf, endereco_id, email, estado_civil_id } = req.body;
        const constituinte = await Constituinte.create({
            nome,
            rg,
            cpf,
            endereco_id,
            email,
            estado_civil_id
        });
        return res.status(200).json(constituinte);
    }

    async index(req, res) {
        const constituintes = await Constituinte.findAll({
            attributes: ["nome", "rg", "cpf", "email"],
            include: [
                {
                    model: Endereco,
                    as: 'endereco',
                    attributes: ['nome', 'numero', 'bairro'],
                },
                {
                    model: EstadoCivil,
                    as: 'estadocivil',
                    attributes: ['nome'],
                },
            ],
        });
        return res.status(200).json(constituintes);
    }

    async show(req, res) {
        const { id } = req.params;
        const constituinte = await Constituinte.findByPk(id, {
            attributes: ['nome', 'rg', 'cpf', 'email'],
            include: [
                {
                    model: Endereco,
                    as: 'endereco',
                    attributes: ['nome', 'numero', 'bairro'],
                },
                {
                    model: EstadoCivil,
                    as: 'estadocivil',
                    attributes: ['nome'],
                },
            ],
        });

        if (!constituinte) {
            return res.status(404).json({
                error: "Constituinte não existe",
            });
        }
        return res.status(200).json(constituinte);
    }

    async update(req,res) {
        const { id } = req.params;
        const { nome, rg, cpf, email } = req.body;
        const constituinte = await Constituinte.findByPk(id);

        if (!constituinte) {
            return res.status(404).json({
                error: "Constituinte não existe",
            });
        }

        const Uconstituinte = await constituinte.update({
            nome,
            rg,
            cpf,
            email
        });
        return res.status(200).json(Uconstituinte);
    }
}

module.exports = new ConstituinteController();