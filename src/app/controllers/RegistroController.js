const Registro = require('../models/Registro');
const Usuario = require('../models/Usuario');
const Constituinte = require('../models/Constituinte');

class RegistroController {
    async store(req, res) {
        const { usuario_id, constituinte_id, registro } = req.body;
        const caso = await Registro.create({
            usuario_id,
            constituinte_id,
            registro
        });
        return res.status(200).json(caso);
    }

    async index(req, res) {
        const registros = await Registro.findAll({
            attributes: ["registro"],
            include: [
                {
                    model: Usuario,
                    as: 'usuario',
                    attributes: ['nome'],
                },
                {
                    model: Constituinte,
                    as: 'constituinte',
                    attributes: ['nome'],
                },
            ],
        });
        return res.status(200).json(registros);
    }

    async show(req, res) {
        const { id } = req.params;
        const caso = await Registro.findByPk(id, {
            attributes: ['registro'],
            include: [
                {
                    model: Usuario,
                    as: 'usuario',
                    attributes: ['nome'],
                },
                {
                    model: Constituinte,
                    as: 'constituinte',
                    attributes: ['nome'],
                },
            ],
        });

        if (!caso) {
            return res.status(404).json({
                error: "Registro não existe",
            });
        }
        return res.status(200).json(caso);
    }

    async update(req, res) {
        const { id } = req.params;
        const { registro } = req.body;
        const caso = await Registro.findByPk(id);

        if (!caso) {
            return res.status(404).json({
                error: "Registro não existe",
            });
        }

        const Ucaso = await caso.update({ registro });
        return res.status(200).json(Ucaso);
    }
}

module.exports = new RegistroController();