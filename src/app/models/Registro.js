const { Sequelize, Model } = require("sequelize");

class Registro extends Model {
    static init(sequelize) {
        super.init(
            {
                registro: Sequelize.TEXT,
                usuario_id: Sequelize.INTEGER,
                constituinte_id: Sequelize.INTEGER,
            },
            {
                sequelize,
                tableName: "registros",
            }
        );
        return this;
    }

    static associate(models) {
        this.belongsTo(models.Usuario, {
            as: 'usuario',
            foreignKey: 'usuario_id',
        });
        this.belongsTo(models.Constituinte, {
            as: 'constituinte',
            foreignKey: 'constituinte_id',
        });
    }
}

module.exports = Registro;