const { Sequelize, Model } = require("sequelize");

class Constituinte extends Model {
    static init(sequelize) {
        super.init(
            {
                nome: Sequelize.STRING,
                endereco_id: Sequelize.INTEGER,
                estado_civil_id: Sequelize.INTEGER,
                rg: Sequelize.STRING,
                cpf: Sequelize.STRING,
                email: Sequelize.STRING,           
            },
            {
                sequelize,
                tableName: "constituintes",
            }
        );

        return this;
    }

    static associate(models) {
        this.belongsTo(models.EstadoCivil, {
            as: 'estadocivil',
            foreignKey: 'estado_civil_id',
        });
        this.belongsTo(models.Endereco, {
            as: 'endereco',
            foreignKey: 'endereco_id',
        });
        this.hasMany(models.Registro, {
            as: 'registro',
            foreignKey: 'constituinte_id',
        });
    }
}

module.exports = Constituinte;